import math
class CambioBase:
    def convertirBase10ABaseN(self, numero,  baseDestino):
        if(numero==0):
            return 0
        if(numero==1):
            return 1
        auxiliar=""
        if(numero<baseDestino):
        	return numero
        while(numero>=baseDestino):
        	auxiliar=auxiliar+str(numero%baseDestino)
        	numero=int(numero/baseDestino)
        auxiliar+=str(numero)
        return int(auxiliar[::-1])

    
    def convertirBaseNA10(self,numero, baseActual):
        if(numero==0):
            return 0
        if(numero==1):
            return 1
        retorno = 0
        index=0
        if(baseActual==16):
            return convertirBase16ABase10(numero,16)
        for x in str(numero)[::-1]:
            retorno +=  (int(x)*(pow(baseActual,index)))
            index+=1
        return retorno
    
    def convertirBaseNAM(self,numero, baseActual,baseDestino):
    	if(baseActual == baseDestino):
    		return numero
    	if(baseActual==16):
    		numeroDecimal=self.hexadecimalADecimal(numero)
    	else:
    		numeroDecimal=self.convertirBaseNA10(int(numero),baseActual)
    		if(baseDestino==16):
    			return self.decimalAHexadecimal(int(numeroDecimal))
    	return self.convertirBase10ABaseN(numeroDecimal,baseDestino)
    
    def decimalAHexadecimal(self,numero):
    	if(numero==0):
    		return 0
    	if(numero==1):
    		return 1
    	if(numero<=9):
    		return numero
    	resultado=""
    	while(numero>=16):
    		auxiliar=numero%16
    		print(auxiliar)
    		numero=math.floor(numero/16)
    		resultado+=self.valoresHexadecimal(auxiliar)
    	resultado+=self.valoresHexadecimal(numero)
    	return resultado[::-1]

    def valoresHexadecimal(self,auxiliar):
    	if(auxiliar==10):
    		return"A"
    	if(auxiliar==11):
    		return"B"
    	if(auxiliar==12):
    		return"C"
    	if(auxiliar==13):
    		return"D"
    	if(auxiliar==14):
    		return"E"
    	if(auxiliar==15):
    		return"F"
    	if(auxiliar==16):
    		return"G"
    	return str(auxiliar)
    def hexadecimalADecimal(self,numero):
    	if(numero==0):
    		return 0
    	if(numero==1):
    		return 1
    	retorno = 0
    	index=0
    	for x in numero[::-1]:
    		retorno +=  (self.valoresDecimal(x)*(pow(16,index)))
    		index+=1
    	return retorno
    def valoresDecimal(self,auxiliar):
    	if(auxiliar=="A"):
    		return 10
    	if(auxiliar=="B"):
    		return 11
    	if(auxiliar=="C"):
    		return 12
    	if(auxiliar=="D"):
    		return 13
    	if(auxiliar=="E"):
    		return 14
    	if(auxiliar=="F"):
    		return 15
    	if(auxiliar=="G"):
    		return 16
    	return int(auxiliar)
cambio = CambioBase()

print("entra")
print(str(cambio.convertirBase10ABaseN(14,2)))
print(str(cambio.convertirBaseNA10(1110,2)))
print (str(cambio.convertirBaseNAM(16,8,2)))
	