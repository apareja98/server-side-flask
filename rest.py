from flask import Flask, jsonify, request

from calculadora import CambioBase
from flask_cors import CORS, cross_origin
app = Flask(__name__)



cambio = CambioBase()

@app.route('/')
@cross_origin()
def index():
	return 'this is the homepage'
@app.route('/cambioBase/<string:numero>/<int:baseActual>', methods=['GET'])
@cross_origin()
def cambioBase(numero,baseActual):
	return jsonify({'binario':cambio.convertirBaseNAM(numero,baseActual,2),
		'octal':cambio.convertirBaseNAM(numero,baseActual,8),
		'decimal':cambio.convertirBaseNAM(numero,baseActual,10),
		'hexadecimal':cambio.convertirBaseNAM(numero,baseActual,16)})
@app.route('/hexa/<string:s>', methods=['GET'])
@cross_origin()
def hexa(s):
	return jsonify({'decimal':cambio.hexadecimalADecimal(s)})

if __name__ == '__main__':
	app.run(debug=True)